package com.seiha.article.bookaritcle.service;

import com.seiha.article.bookaritcle.model.Article;
import com.seiha.article.bookaritcle.model.ArticleFilter;

import java.util.List;

public interface ArticleService {
    void insert(Article article);
    void update(Article article);
    void delete(int id);
    Article findOne(int id);
    List<Article> findAll();

    List<Article> findAllFilter(ArticleFilter filter);
}
