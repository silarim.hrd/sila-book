package com.seiha.article.bookaritcle.model;

public class Category {
    private int id;
    private String cateTitle;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCateTitle() {
        return cateTitle;
    }

    public void setCateTitle(String cateTitle) {
        this.cateTitle = cateTitle;
    }

    public Category(){}

    public Category(int id, String cateTitle) {
        this.id = id;
        this.cateTitle = cateTitle;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", cateTitle='" + cateTitle + '\'' +
                '}';
    }
}
