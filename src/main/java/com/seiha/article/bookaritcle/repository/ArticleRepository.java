package com.seiha.article.bookaritcle.repository;

import com.seiha.article.bookaritcle.repository.provider.ArticleProvider;
import com.seiha.article.bookaritcle.model.Article;
import com.seiha.article.bookaritcle.model.ArticleFilter;
import org.apache.ibatis.annotations.*;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@MapperScan("com.seiha.article.bookaritcle.repository")
public interface ArticleRepository {

    @Insert("INSERT INTO tb_articles(title, description, author, thumbnail, created_date, category_id)"+
            " VALUES(#{title},#{description}, #{author}, #{thumbnail}, #{createdDate},#{category.id})")
    public boolean insert(Article article);

    @Select("SELECT a.id, a.title, a.description, a.author, a.thumbnail, a.created_date, a.category_id, c.name from tb_articles a Inner join tb_categories c ON a.category_id = c.id WHERE a.id=#{id}")
//    @Select("select a.id, a.title, a.description, a.author, a.thumbnail, a.created_date, a.category_id, c.category from tb_articles a Inner join tb_categories c On a.category_id = c.id")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "description", column = "description"),
            @Result(property = "author", column = "author"),
            @Result(property = "thumbnail", column = "thumbnail"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "category.id", column = "category_id"),
            @Result(property = "category.cateTitle", column = "name")
    })
    public Article findOne(int id);

    @Select("SELECT a.id, a.title, a.description, a.author, a.thumbnail," +
            " a.created_date, a.category_id, c.name " +
            "from tb_articles a Inner join tb_categories c On a.category_id = c.id ORDER BY a.id ASC")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "description", column = "description"),
            @Result(property = "author", column = "author"),
            @Result(property = "thumbnail", column = "thumbnail"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "category.id", column = "category_id"),
            @Result(property = "category.cateTitle", column = "name")
    })
    public List<Article> findAll();
    @Update("update tb_articles set title=#{title}, description=#{description}, author=#{author}, category_id=#{category.id} where id=#{id}")
    public boolean update(Article article);
    @Delete("delete from tb_articles where id=#{id}")
    public boolean delete(int id);

    @SelectProvider(method = "findAllFilter", type =ArticleProvider.class)
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "description", column = "description"),
            @Result(property = "author", column = "author"),
            @Result(property = "thumbnail", column = "thumbnail"),
            @Result(property = "createdDate", column = "created_date"),
            @Result(property = "category.id", column = "category_id"),
            @Result(property = "category.cateTitle", column = "name")
    })
    public List<Article> findAllFilter(ArticleFilter filter);


}
