package com.seiha.article.bookaritcle.repository.category;

import com.seiha.article.bookaritcle.model.Category;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CategoryRepositoryImplement implements CategoryRepository {

    private List<Category> categories = new ArrayList<>();
    public CategoryRepositoryImplement(){
//        categories.add(new Category(1, "IOS"));
//        categories.add(new Category(2, "Spring"));
//        categories.add(new Category(3, "Mobile"));
//        categories.add(new Category(4, "Web Developer"));
//        categories.add(new Category(5, "Java"));
    }
    @Override
    public List<Category> findAll() {

        return categories;
    }

    @Override
    public Category findOne(int id) {
        for (Category cate: categories){
            if (cate.getId()== id){
                return cate;
            }
        }
        return null;
    }
}
