package com.seiha.article.bookaritcle.repository.category;

import com.seiha.article.bookaritcle.model.Category;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface CategoryRepository {

    @Select("SELECT * FROM tb_categories")
    @Results({@Result (property = "cateTitle", column = "name")})
    public List<Category> findAll();

    @Select("select * from tb_categories where id = #{id}")
    @Results({@Result(property = "cateTitle", column = "name")})
    public Category findOne(int id);
}
