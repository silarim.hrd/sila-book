package com.seiha.article.bookaritcle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeilaAritcleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SeilaAritcleApplication.class, args);
    }
}
