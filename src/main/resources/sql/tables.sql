create table tb_categories(
id INT primary key auto_increment,
name varchar not null);

create table tb_articles(
id INT primary key auto_increment,
title varchar not null,
description text not null,
author varchar(50),
thumbnail varchar not null,
created_date timestamp default now(),
category_id int not null References tb_categories(id) on delete cascade on update cascade
);